UIApplication *app = [UIApplication sharedApplication];
__block UIBackgroundTaskIdentifier bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (bgTask != UIBackgroundTaskInvalid) {
            [app endBackgroundTask:bgTask];
            bgTask = UIBackgroundTaskInvalid;
        }
    });
}];
dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
 
        testTimer = [NSTimer scheduledTimerWithTimeInterval:(0.01) target:self selector:@selector(doSomeTest) userInfo:nil repeats:YES];
        [testTimer fire];
        [[NSRunLoop currentRunLoop] addTimer:testTimer forMode:NSRunLoopCommonModes];
         
        [[NSRunLoop currentRunLoop] run];
         
        dispatch_async(dispatch_get_main_queue(), ^{
            if (bgTask != UIBackgroundTaskInvalid) {
                [app endBackgroundTask:bgTask];
                bgTask = UIBackgroundTaskInvalid;
            }
        });
});